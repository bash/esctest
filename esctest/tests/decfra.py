import tests.fill_rectangle
import esccmd
from escutil import knownBug

CHARACTER = "%"

class DECFRATests(tests.fill_rectangle.FillRectangleTests):
  def fill(self, top=None, left=None, bottom=None, right=None):
    esccmd.DECFRA(str(ord(CHARACTER)), top, left, bottom, right)

  def characters(self, point, count):
    return CHARACTER * count

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  def test_DECFRA_basic(self):
    self.fillRectangle_basic()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.", noop=True)
  def test_DECFRA_invalidRectDoesNothing(self):
    self.fillRectangle_invalidRectDoesNothing()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  def test_DECFRA_defaultArgs(self):
    self.fillRectangle_defaultArgs()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  def test_DECFRA_respectsOriginMode(self):
    self.fillRectangle_respectsOriginMode()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  def test_DECFRA_overlyLargeSourceClippedToScreenSize(self):
    self.fillRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.", noop=True)
  def test_DECFRA_cursorDoesNotMove(self):
    self.fillRectangle_cursorDoesNotMove()

  @knownBug(terminal="vte", reason="DECFRA not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  def test_DECFRA_ignoresMargins(self):
    self.fillRectangle_ignoresMargins()
