import esccmd
import escio
from escutil import knownBug, AssertCellMatchesSGRParams
from esctypes import Point

class SGRTests(object):

  @classmethod
  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_Bold(cls):
    """Tests bold."""
    escio.Write("x")
    esccmd.SGR(esccmd.SGR_BOLD)
    escio.Write("y")
    AssertCellMatchesSGRParams(
	Point(1, 1),
        [],
	[esccmd.SGR_BOLD])
    AssertCellMatchesSGRParams(
	Point(2, 1),
	[esccmd.SGR_BOLD])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_Strikethrough(self):
    """Tests strikethrough."""
    escio.Write("x")
    esccmd.SGR(esccmd.SGR_STRIKETHROUGH)
    escio.Write("y")
    AssertCellMatchesSGRParams(
	Point(1, 1),
        [],
	[esccmd.SGR_STRIKETHROUGH])
    AssertCellMatchesSGRParams(
	Point(2, 1),
	[esccmd.SGR_STRIKETHROUGH])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_Overline(self):
    """Tests overline."""
    escio.Write("x")
    esccmd.SGR(esccmd.SGR_OVERLINE)
    escio.Write("y")
    AssertCellMatchesSGRParams(
	Point(1, 1),
        [],
	[esccmd.SGR_OVERLINE])
    AssertCellMatchesSGRParams(
	Point(2, 1),
	[esccmd.SGR_OVERLINE])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_Italic(self):
    """Tests italic text."""
    escio.Write("x")
    esccmd.SGR(esccmd.SGR_ITALIC)
    escio.Write("y")
    AssertCellMatchesSGRParams(
	Point(1, 1),
        [],
	[esccmd.SGR_ITALIC])
    AssertCellMatchesSGRParams(
	Point(2, 1),
	[esccmd.SGR_ITALIC])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_256Color_Foreground(self):
    """Tests 256 color foreground color support."""
    colors = [16, 232, 255]
    for color in colors:
      esccmd.SGR(esccmd.SGR_RESET, esccmd.SGR_FOREGROUND, esccmd.SGR_256, color)
      escio.Write("x")
    x = 1
    for color in colors:
      AssertCellMatchesSGRParams(
        Point(x, 1),
        [[esccmd.SGR_FOREGROUND, esccmd.SGR_256, color]])
      x += 1

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_256Color_Background(self):
    """Tests 256 color background color support."""
    colors = [16, 232, 255]
    for color in colors:
      esccmd.SGR(esccmd.SGR_RESET, esccmd.SGR_BACKGROUND, esccmd.SGR_256, color)
      escio.Write("x")
    x = 1
    for color in colors:
      AssertCellMatchesSGRParams(
        Point(x, 1),
        [[esccmd.SGR_BACKGROUND, esccmd.SGR_256, color]])
      x += 1

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_24BitCompat_Foreground(self):
    """Tests 24 bit color foreground color support."""
    rgb = [0, 101, 255]
    esccmd.SGR(
        esccmd.SGR_RESET,
        esccmd.SGR_FOREGROUND,
        esccmd.SGR_24BIT,
        rgb[0],
        rgb[1],
        rgb[2])
    escio.Write("x")
    AssertCellMatchesSGRParams(
        Point(1, 1),
        [[esccmd.SGR_FOREGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE] +
          rgb])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_24BitCompat_Background(self):
    """Tests 24 bit color background color support."""
    rgb = [0, 101, 255]
    esccmd.SGR(
        esccmd.SGR_RESET,
        esccmd.SGR_BACKGROUND,
        esccmd.SGR_24BIT,
        rgb[0],
        rgb[1],
        rgb[2])
    escio.Write("x")
    AssertCellMatchesSGRParams(
        Point(1, 1),
        [[esccmd.SGR_BACKGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE] +
          rgb])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_24BitITU_Foreground(self):
    """Tests 24-bitcolor foreground color support using the ITU style code."""
    rgb = [0, 101, 255]
    esccmd.SGR(
        esccmd.SGR_RESET,
        [ esccmd.SGR_FOREGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE,
          rgb[0],
          rgb[1],
          rgb[2],
          "1" ])
    escio.Write("x")
    AssertCellMatchesSGRParams(
        Point(1, 1),
        [[esccmd.SGR_FOREGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE] +
          rgb])

  @knownBug(terminal="vte", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="wezterm", reason="XTREPORTSGR not implemented", shouldTry=False)
  @knownBug(terminal="konsole", reason="XTREPORTSGR not implemented", shouldTry=False)
  def test_SGR_24BitITU_Background(self):
    """Tests 24-bitcolor background color support using the ITU style code."""
    rgb = [0, 101, 255]
    esccmd.SGR(
        esccmd.SGR_RESET,
        [ esccmd.SGR_BACKGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE,
          rgb[0],
          rgb[1],
          rgb[2],
          "1" ])
    escio.Write("x")
    AssertCellMatchesSGRParams(
        Point(1, 1),
        [[esccmd.SGR_BACKGROUND,
          esccmd.SGR_24BIT,
          esccmd.SGR_DEFAULT_COLORSPACE] +
          rgb])

