import esccmd
from escutil import knownBug
from tests.save_restore_cursor import SaveRestoreCursorTests

class SCORCTests(SaveRestoreCursorTests):
  def __init__(self):
    SaveRestoreCursorTests.__init__(self)

  @classmethod
  def saveCursor(cls):
    esccmd.SCOSC()

  @classmethod
  def restoreCursor(cls):
    esccmd.SCORC()

  def test_SaveRestoreCursor_Basic(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_Basic(self)

  def test_SaveRestoreCursor_MoveToHomeWhenNotSaved(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_MoveToHomeWhenNotSaved(self)

  def test_SaveRestoreCursor_ResetsOriginMode(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_ResetsOriginMode(self)

  @knownBug(terminal="konsole", reason="DECSLRM not implemented.")
  def test_SaveRestoreCursor_WorksInLRM(self, shouldWork=True):
    SaveRestoreCursorTests.test_SaveRestoreCursor_WorksInLRM(self, False)

  def test_SaveRestoreCursor_AltVsMain(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_AltVsMain(self)

  def test_SaveRestoreCursor_Protection(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_Protection(self)

  def test_SaveRestoreCursor_Wrap(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_Wrap(self)

  def test_SaveRestoreCursor_ReverseWrapNotAffected(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_ReverseWrapNotAffected(self)

  def test_SaveRestoreCursor_InsertNotAffected(self):
    SaveRestoreCursorTests.test_SaveRestoreCursor_InsertNotAffected(self)
