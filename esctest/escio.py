import os
import select
import sys
import tty
from termios import tcflush, TCIOFLUSH

from esc import ESC, BEL, ST_C0
import escargs
from esclog import LogDebug
import esctypes
import escoding

stdin_fd = None
stdout_fd = None
gSideChannel = None
gSideChannelOutput = None
use8BitControls = False

def Init():
  global stdout_fd
  global stdin_fd

  stdout_fd = os.fdopen(sys.stdout.fileno(), 'wb', 0)
  stdin_fd = os.fdopen(sys.stdin.fileno(), 'rb', 0)
  tty.setraw(stdin_fd)

def Shutdown():
  tty.setcbreak(stdin_fd)

def Write(s, sideChannelOk=True):
  global gSideChannel
  if sideChannelOk and gSideChannel is not None:
    gSideChannel.write(escoding.to_binary(s))
  stdout_fd.write(escoding.to_binary(s))

def SetSideChannel(inputFilename, outputFilename):
  global gSideChannel
  global gSideChannelOutput
  if inputFilename is None:
    if gSideChannel:
      gSideChannel.close()
      gSideChannel = None
  else:
    gSideChannel = open(inputFilename, "wb")
  if outputFilename is None:
    if gSideChannelOutput:
      gSideChannelOutput.close()
      gSideChannelOutput = None
  else:
    gSideChannelOutput = open(outputFilename, "wb")

def APC():
  """Application Program Command."""
  if use8BitControls:
    return chr(0x9f)
  return ESC + "_"

def OSC():
  """Operating System Command."""
  if use8BitControls:
    return chr(0x9d)
  return ESC + "]"

def PM():
  """Privacy message."""
  if use8BitControls:
    return chr(0x9e)
  else:
    return ESC + "^"

def SOS():
  """Start of string."""
  if use8BitControls:
    return chr(0x98)
  else:
    return ESC + "X"

def ST():
  """String Terminator."""
  if use8BitControls:
    return chr(0x9c)
  else:
    return ESC + "\\"

def CSI():
  """Control Sequence Introducer."""
  if use8BitControls:
    return chr(0x9b)
  return ESC + "["

def DCS():
  """Device Control String."""
  if use8BitControls:
    return chr(0x90)
  return ESC + "P"

def WriteAPC(params, bel=False, requestsReport=False):
  str_params = list(map(str, params))
  if bel:
    terminator = BEL
  else:
    terminator = ST()
  sequence = APC() + "".join(str_params) + terminator
  LogDebug("Send sequence: " + sequence.replace(ESC, "<ESC>"))
  Write(sequence, sideChannelOk=not requestsReport)

def WriteOSC(params, bel=False, requestsReport=False):
  str_params = list(map(str, params))
  joined_params = ";".join(str_params)

  if bel:
    terminator = BEL
  else:
    terminator = ST()
  sequence = OSC() + joined_params + terminator
  LogDebug("Send sequence: " + sequence.replace(ESC, "<ESC>"))
  Write(sequence, sideChannelOk=not requestsReport)

def WriteDCS(introducer, params):
  Write(DCS() + introducer + params + ST())

def WriteCSI(prefix="", params=[], intermediate="", final="", requestsReport=False):
  if len(final) == 0:
    raise esctypes.InternalError("final must not be empty")
  def StringifyCSIParam(p):
    if p is None:
      return ""
    elif type(p) is list:
      return ":".join(map(str, p))
    return str(p)
  str_params = list(map(StringifyCSIParam, params))

  # Remove trailing empty args
  while len(str_params) > 0 and str_params[-1] == "":
    str_params = str_params[:-1]

  joined_params = ";".join(str_params)
  sequence = CSI() + prefix + joined_params + intermediate + final
  LogDebug("Send sequence: " + sequence.replace(ESC, "<ESC>"))
  Write(sequence, sideChannelOk=not requestsReport)

def ReadFromStdin():
  c = read(1)
  global gSideChannelOutput
  if gSideChannelOutput is not None:
    gSideChannelOutput.write(c)
  return c

def ReadOrDie(e):
  c = ReadFromStdin()
  AssertCharsEqual(c, e)

def AssertCharsEqual(c, e):
  if c != e:
    raise esctypes.InternalError("Read %c (0x%02x), expected %c (0x%02x)" % (c, ord(c), e, ord(e)))

def ReadOSC(expected_prefix):
  """Read an OSC code starting with |expected_prefix|."""
  ReadOrDie(ESC)
  ReadOrDie(']')
  for c in expected_prefix:
    ReadOrDie(c)
  s = ""
  st = ST()
  while not s.endswith(st):
    c = ReadFromStdin()
    s += c
  return s[:-2]

def CSIParam(value):
  """If value is an integer, return an int. If it is integers delimited by
  colons, return a list of integers. Empty params will be None. Otherwise throw
  an exception."""
  if len(value) == 0:
    return None
  parts = value.split(":")
  ints = list(map(int, parts))
  if len(ints) == 1:
    return ints[0]
  return ints

def ReadParams(expected_final, expected_prefix=None):
  if expected_prefix is not None:
    for c in expected_prefix:
      ReadOrDie(c)

  params = []
  current_param = ""

  c = ReadFromStdin()
  while True:
    if c == ";":
      params.append(CSIParam(current_param))
      current_param = ""
    elif '0' <= c <= '9':
      current_param += c
    elif c == ':':
      current_param += c
    else:
      # Read all the final characters, asserting they match.
      while True:
        AssertCharsEqual(c, expected_final[0])
        expected_final = expected_final[1:]
        if len(expected_final) > 0:
          c = ReadFromStdin()
        else:
          break

      if current_param != "" or len(params) > 0:
        params.append(CSIParam(current_param))
      break
    c = ReadFromStdin()
  return params

def ReadCSI(expected_final, expected_prefix=None):
  """Read a CSI code ending with |expected_final| and returns an array of parameters. """

  c = ReadFromStdin()
  if c == ESC:
    ReadOrDie('[')
  elif ord(c) != 0x9b:
    raise esctypes.InternalError("Read %c (0x%02x), expected CSI" % (c, ord(c)))

  params = ReadParams(expected_final, expected_prefix)
  LogDebug("ReadCSI parameters: " + ";".join(map(str,params)))
  return params

def ReadDCS(expected_final, expected_prefix=None):
  """ Read a DCS code. Returns the characters between DCS and ST, and the params."""
  p = ReadFromStdin()
  if p == ESC:
    ReadOrDie("P")
    p += "P"
  elif ord(p) == 0x90:
    p = "<DCS>"
  else:
    raise esctypes.InternalError("Read %c (0x%02x), expected DCS" % (p, ord(p)))

  params = ReadParams(expected_final, expected_prefix)

  result = ""
  st = ST_C0
  while not result.endswith(st) and not result.endswith(chr(0x9c)):
    c = ReadFromStdin()
    result += c
  LogDebug("Read response: " + (p + result).replace(ESC, "<ESC>"))
  if result.endswith(st):
    return [params, result[:-2]]
  return [params, result[:-1]]

def read(n):
  """Try to read n bytes. Times out if it takes more than 1
  second to read any given byte."""
  s = ""
  f = sys.stdin.fileno()
  for _ in range(n):
    r, w, e = select.select([f], [], [], escargs.args.timeout)
    if f not in r:
      raise esctypes.InternalError("Timeout waiting to read.")
    s += escoding.to_string(os.read(f, 1))
  return s

def Flush():
  tcflush(sys.stdin, TCIOFLUSH)
